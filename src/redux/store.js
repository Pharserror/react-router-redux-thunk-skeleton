import { configureStore } from '@reduxjs/toolkit';
import reducer from '@src/redux/reducers';

// type Actions = typeof keyof reducer;

export default configureStore(reducer);
