const INITIAL_STATE = {
  alerts: {
    errors: [],
    messages: [],
  },
  data: {},
  meta: {},
};

export default INITIAL_STATE;
