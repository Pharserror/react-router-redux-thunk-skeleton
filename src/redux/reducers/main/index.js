import clone         from 'lodash/clone';
import invoke        from 'lodash/invoke';
import * as actions  from './actions';

export const INITIAL_STATE = {
  alerts: {
    errors: [],
    messages: [],
  },
  data: {},
  meta: {},
};

export default function MainReducer(
  state = INITIAL_STATE,
  // TODO: Put alerts, data, and meta into payload
  action, // { alerts, data, meta, payload, type },
) {
  const newState = clone(state);

  return invoke(actions, action.type, newState, action.payload) || state;
}
