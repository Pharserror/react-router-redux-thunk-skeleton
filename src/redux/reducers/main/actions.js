import get   from 'lodash/get';
import merge from 'lodash/merge';
import i18n  from '@src/i18n';

export function ON_AJAX_ERROR(state, payload) {
  const message = i18n.t(`http.${payload.status}`) || i18n.t('http.500') || '';

  return merge(
    state,
    payload,
    { alerts: { errors: [...get(payload, 'alerts.errors', [])] } },
  );
}

export function ON_PARSE_DATA_INTO_STORE(state, payload) {
  return merge({}, state, payload);
}
