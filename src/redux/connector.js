import { connect } from 'react-redux';
import keys from 'lodash/keys';
import reduce from 'lodash/reduce';
import INITIAL_STATE from './reducers/INITIAL_STATE';

export default function connector(Component, props) {
  return connect(
    reducers => ({
      ...reduce(keys(INITIAL_STATE), (state, key) => ({
        ...state,
        [key]: reducers.MainReducer[key],
      }), {}),
      extra: props,
    }),
    dispatch => ({ dispatch })
  )(Component);
}
