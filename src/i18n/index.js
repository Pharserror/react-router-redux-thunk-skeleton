import i18n from 'i18next';
import { useEffect } from 'react';
import en from './en';

export const DEFAULT_LOCALE = 'en';
export const DEFAULT_NAMESPACE = 'translation';

export function initializeI18n(locale = DEFAULT_LOCALE) {
  return i18n.init({
    lng: locale,
    resources: {
      [DEFAULT_LOCALE]: {
        [DEFAULT_NAMESPACE]: {
        },
      },
    },
  });
}

export async function loadResourceBundle({
  importPath,
  language,
  resource,
  translationKey,
  namespace = DEFAULT_NAMESPACE,
}) {
  if (!!importPath && !!translationKey && !i18n.exists(translationKey)) {
    const resolvedLanguage = language || i18n.language || DEFAULT_LOCALE;
    const fileName = `${resource || resolvedLanguage}.json`;
    const filePath = `${importPath}/${fileName}`;
    const resourceBundle = await import(`@src/locales/${filePath}`);

    i18n.addResourceBundle(
      resolvedLanguage,
      namespace,
      resourceBundle,
      true,
      true,
    );
  }
}

export function useI18n(options) {
  useEffect(() => {
    loadResourceBundle(options);
  }, []);
}
