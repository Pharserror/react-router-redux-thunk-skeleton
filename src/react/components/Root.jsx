import { Provider } from 'react-redux';
import React from 'react';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import connector from '@src/redux/connector';
import ScreenConductor from './ScreenConductor';

export default function Root({ store }) {
  return (
    <div id="provider_wrapper">
      <Provider store={store}>
        <Router>
          <Route path="/" component={connector(ScreenConductor)} />
        </Router>
      </Provider>
    </div>
  );
}
