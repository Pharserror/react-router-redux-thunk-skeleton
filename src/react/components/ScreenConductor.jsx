import isEmpty from 'lodash/isEmpty';
import { Link, Route } from 'react-router-dom';
import React from 'react';
import connector from '@src/redux/connector';
import Footer from './Footer';
import HelloWorld from './HelloWorld';
import Loader from './Loader';
import SitewideNav from './SitewideNav';

export default function ScreenConductor({ data, dispatch }) {
  return (
    process.env.APP_HYDRATION_URL && isEmpty(data)
    ? (() => {
      findOrLoadResources({
        dispatch,
        url: process.env.APP_HYDRATION_URL
      });

      return <Loader />;
    })() : (
      <div>
        <SitewideNav />
        <Route exact path="/" component={connector(HelloWorld)} />
        <Footer />
      </div>
    )
  );
};
