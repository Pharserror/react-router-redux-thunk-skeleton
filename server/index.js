var express = require('express');
var path = require('path');
var webpack = require('webpack');
var config = require('../webpack.config');
var app = express();
var compiler = webpack(config);

// Tell Express to use webpack if we are in a dev environment
if (process.env.NODE_ENV === 'development') {
  console.log('Using webpack-dev-middleware');
  app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath
  }));
}

app.use('/stylesheets', express.static(path.join(__dirname, '../client/stylesheets')));

app.get('/main.css', function(req, res) {
  res.sendFile(path.join(__dirname, '../client/main.css'));
});

app.get('/bootstrap.css', function(req, res) {
  res.sendFile(path.join(__dirname, '../node_modules/bootstrap/dist/css/bootstrap.css'));
});

app.get('/bootstrap.css.map', function(req, res) {
  res.sendFile(path.join(__dirname, '../node_modules/bootstrap/dist/css/bootstrap.css.map'));
});

app.get('/es6-promise.map', function(req, res) {
  res.sendFile(path.join(__dirname, '../node_modules/es6-promise/dist/es6-promise.map'));
});

app.get('/app.js', function(req, res) {
  res.sendFile(path.join(__dirname, '../public/javascripts/app.js'));
});

app.get(/\d\.bundle\.js/, function(req, res) {
  res.sendFile(path.join(__dirname, '../public/javascripts/' + req.path));
});

app.get(/loader.gif/, function(req, res) {
  res.sendFile(path.join(__dirname, '../node_modules/selectr/app/assets/images/loader.gif'));
});

app.get(/^\/images.*/, function(req, res) {
  res.sendFile(path.join(__dirname, '../public/assets/' + req.path));
});

app.get(/^\/(?!images).*/, function(req, res) {
  res.sendFile(path.join(__dirname, '../client/main.html'));
});

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, '../client/main.html'));
});

app.listen(process.env.EXPRESS_PORT, process.env.EXPRESS_HOST, function(err) {
  if (err) {
    return console.error(err);
  }

  console.log(`Listening at ${process.env.EXPRESS_HOST}:${process.env.EXPRESS_PORT}`);
});
