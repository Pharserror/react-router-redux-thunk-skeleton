module.exports = {
  collectCoverageFrom: [
    'src/**/*.{js,jsx}',
    '!**/node_modules/**',
  ],
  moduleFileExtensions: [
    'js',
    'json',
    'jsx',
  ],
  moduleNameMapper: {
    "^@src(.*)$": "<rootDir>/src$1",
  },
  setupFiles: ['<rootDir>/jest.setup.js'],
  testMatch: [
    '**/__tests__/**/*test.js?(x)',
    '**/?(*.)+(spec|test).js?(x)',
  ],
  testPathIgnorePatterns: [
    '<rootDir>/cypress/*',
    '/node_modules/',
  ],
  transform: {
    '.*': '<rootDir>/node_modules/babel-jest'
  },
  verbose: true,
};
