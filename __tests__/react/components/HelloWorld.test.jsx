import React from 'react';
import { render, screen } from '@testing-library/react';
import HelloWorld from '@src/react/components/HelloWorld';

describe('HelloWorld', () => {
  it('renders', () => {
    const { container } = render( <HelloWorld /> );

    expect(container).toMatchSnapshot();
  });
});
