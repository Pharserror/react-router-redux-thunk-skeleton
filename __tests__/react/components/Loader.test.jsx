import React from 'react';
import { render, screen } from '@testing-library/react';
import Loader from '@src/react/components/Loader';

describe('Loader', () => {
  it('renders', () => {
    const { container } = render( <Loader /> );

    expect(container).toMatchSnapshot();
  });
});
