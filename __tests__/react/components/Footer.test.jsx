import React from 'react';
import { render, screen } from '@testing-library/react';
import Footer from '@src/react/components/Footer';

describe('Footer', () => {
  it('renders', () => {
    const { container } = render( <Footer /> );

    expect(container).toMatchSnapshot();
  });
});
