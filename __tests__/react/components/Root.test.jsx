import React from 'react';
import { render, screen } from '@testing-library/react';
import Root from '@src/react/components/Root';
import store from '@src/redux/store';

describe('Root', () => {
  it('renders', () => {
    const { container } = render( <Root /> );

    expect(container).toMatchSnapshot();
  });

  describe('with a store', () => {
    test('renders', () => {
      const { container } = render( <Root store={store} /> );

      expect(container).toMatchSnapshot();
    });
  });
});
