import React from 'react';
import { render, screen } from '@testing-library/react';
import SitewideNav from '@src/react/components/SitewideNav';

describe('SitewideNav', () => {
  it('renders', () => {
    const { container } = render( <SitewideNav /> );

    expect(container).toMatchSnapshot();
  });
});
