import React from 'react';
import { render, screen } from '@testing-library/react';
import ScreenConductor from '@src/react/components/ScreenConductor';

describe('ScreenConductor', () => {
  it('renders', () => {
    const { container } = render( <ScreenConductor /> );

    expect(container).toMatchSnapshot();
  });
});
