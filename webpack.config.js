const path = require('path');
const webpack = require('webpack');
module.exports = {
  context: __dirname,
  entry: {
    app: [
      './client/main.js',
    ],
  },
  mode: 'development',
  module: {
    rules: [{
      test: /\.jsx?$/,
      loader: 'babel-loader',
    }, {
      test: /\.s?css$/,
      use: ['style-loader', 'css-loader', 'sass-loader'],
    }],
  },
  output: {
    chunkFilename: '[name].bundle.js',
    path: __dirname + '/public/javascripts',
    publicPath: '/',
  },
  plugins: [
    new webpack.EnvironmentPlugin([
      ...(process.env.APP_HYDRATION_URL ? ['APP_HYDRATION_URL'] : []),
    ]),
  ],
  resolve: {
    alias: {
      '@src': path.join(__dirname, 'src'),
    },
    extensions: ['.js', '.jsx'],
  },
};
